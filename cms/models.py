import json
from django.db import models

class EinkaufslisteModel(models.Model):
    title = models.CharField('Title', max_length=200, blank=False)	
    price = models.DecimalField('price',max_digits=7, decimal_places=2, default=0.00)

    def __str__(self):
        return self.title
        
class SenderlisteModel(models.Model):        
    nr = models.CharField('nr', max_length=20, blank=False, default='000')	
    title = models.CharField('Title', max_length=200, blank=False)	
    link = models.CharField('Link', max_length=200, blank=False)	

    def __str__(self):
        return self.title

#    def save(self, commit=True):
#        return super(SenderlisteModel, self).save(commit)        

class AboutModel(models.Model):
    with open("version.json", "r") as read_file:
      data = json.load(read_file)
      versnr=data["versnr"]	
      versdat=data["versdat"]

customized_sitetree_models = False

if customized_sitetree_models:

    from sitetree.models import TreeItemBase, TreeBase


    class MyTree(TreeBase):

        custom_field = models.CharField('Custom tree field', max_length=50, null=True, blank=True)


    class MyTreeItem(TreeItemBase):

        custom_field = models.IntegerField('Custom item field', default=42)
