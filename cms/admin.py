from django.contrib import admin
from .models import EinkaufslisteModel, SenderlisteModel

class SenderModelAdmin(admin.ModelAdmin):
    list_display = ('nr','title',)
    ordering = ('nr',)


admin.site.register(EinkaufslisteModel)
admin.site.register(SenderlisteModel, SenderModelAdmin)

