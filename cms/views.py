from __future__ import unicode_literals
from django.shortcuts import render, get_list_or_404, redirect, get_object_or_404
from sitetree.toolbox import register_i18n_trees
from .utils import render_themed
from .models import AboutModel, EinkaufslisteModel, SenderlisteModel


register_i18n_trees(['main'])

def ViewHome(request):
    return render_themed(request, 'home', {})
    
def ViewAbout(request):
    context={
        'versnr':AboutModel.versnr,
        'versdat':AboutModel.versdat}	
    return render_themed(request, 'about', context)	
    	
def ViewEinkaufsliste(request):
    einkaufid = request.POST.get('einkaufid', None)
    if 'loeschen' in request.POST:
        EinkaufslisteModel.objects.filter(pk=einkaufid).delete()
    if 'speichern' in request.POST:
        title = request.POST.get('title', '')
        neu = request.POST.get('neu', 'N')
        if neu=="J":
            einkauf=EinkaufslisteModel.objects.create(title=title)
        else:        	    		
            einkauf=EinkaufslisteModel(id=einkaufid,title=title)
        einkauf.save()
    return render_themed(request, 'einkaufsliste', {'einkaufsliste': get_list_or_404(EinkaufslisteModel)})

def ViewEinkaufdetail(request, einkauf_id=0):
    einkauf = get_object_or_404(EinkaufslisteModel,id=einkauf_id)	
    return render_themed(request, 'einkaufdetail', {'einkauf':einkauf})

def ViewEinkaufinsert(request):
    return render_themed(request, 'einkaufdetail', {})

def ViewEinkaufdelete(request, einkauf_id=0):
    einkauf = get_object_or_404(EinkaufslisteModel,id=einkauf_id)	
    return render_themed(request, 'einkaufdelete', {'einkauf':einkauf})


def ViewSenderliste(request):
    senderid = request.POST.get('senderid', 0)    		
    if 'loeschen' in request.POST:
        SenderlisteModel.objects.filter(pk=senderid).delete()
    if 'speichern' in request.POST:
        nr = request.POST.get('nr', '')    		
        title = request.POST.get('title', '')    		
        link = request.POST.get('link', '')
        neu = request.POST.get('neu', 'N')
        if neu=="J":
            sender=SenderlisteModel.objects.create(nr=nr,title=title,link=link)
        else:        	    		
            sender=SenderlisteModel(id=senderid,nr=nr,title=title,link=link)
        sender.save()
    return render_themed(request, 'senderliste', {'senderliste': get_list_or_404(SenderlisteModel.objects.order_by('nr'))})
    
def ViewSenderdetail(request, sender_id=0):
    sender = get_object_or_404(SenderlisteModel,id=sender_id)	
    return render_themed(request, 'senderdetail', {'sender':sender})

def ViewSenderinsert(request):
    return render_themed(request, 'senderdetail', {})

def ViewSenderdelete(request, sender_id=0):
    sender = get_object_or_404(SenderlisteModel,id=sender_id)	
    return render_themed(request, 'senderdelete', {'sender':sender})
    
