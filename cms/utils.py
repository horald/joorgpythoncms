from django.shortcuts import render


def render_themed(request, view_type, context):
    theme = request.theme
    context.update({
        'tpl_head': 'head%s.html' % theme,
        'tpl_body': '%s%s.html' % (view_type, theme),
        'tpl_about': 'about.html'
    })
    return render(request, '%s.html' % view_type, context)
