from django.conf.urls import url
from django.urls import path
from .views import ViewHome, ViewAbout, ViewEinkaufsliste, ViewEinkaufdetail, ViewEinkaufinsert, ViewSenderliste, ViewSenderdetail, ViewSenderdelete, ViewSenderinsert, ViewEinkaufdelete

app_name = 'cms'

urlpatterns = [
  path('', ViewHome, name='home'),
  path('about/', ViewAbout, name='about'),
  path('einkaufsliste/', ViewEinkaufsliste, name='einkaufsliste'),
  path('einkaufdetail/', ViewEinkaufinsert, name='einkaufdetail'),
  path('einkaufdelete/<int:einkauf_id>', ViewEinkaufdelete, name='einkaufdelete'),
  path('einkaufdetail/<int:einkauf_id>/', ViewEinkaufdetail, name='einkaufdetail'),
  path('senderliste/', ViewSenderliste, name='senderliste'),
  path('senderdetail/', ViewSenderinsert, name='senderdetail'),
  path('senderdelete/<int:sender_id>', ViewSenderdelete, name='senderdelete'),
  path('senderdetail/<int:sender_id>/', ViewSenderdetail, name='senderdetail'),
]
